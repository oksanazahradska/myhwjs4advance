fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(response => response.json())
    .then(data => {
        console.log(data);

        const filmList = document.createElement('ul');
        filmList.id = 'film-list';
        document.body.appendChild(filmList);
        data.forEach(film => {
            const listItem = document.createElement('li');
            listItem.innerHTML = `
                <h3>Эпизод ${film.episodeId}: ${film.name}</h3>
                <p>${film.openingCrawl}</p>
                <p>Персонажи:</p>
            `;

    
            filmList.appendChild(listItem);

           
            film.characters.forEach(characterUrl => {
                fetch(characterUrl)
                    .then(response => response.json())
                    .then(characterData => {
                        const characterName = characterData.name;
                        const characterListItem = document.createElement('li');
                        characterListItem.textContent = characterName;
                        listItem.appendChild(characterListItem);
                    })
                    .catch(error => {
                        console.error('Ошибка при получении данных о персонаже:', error);
                    });
            });
        });
    })
    .catch(error => {
        console.error('Ошибка:', error);
    });
